package test;

import com.citadelles.models.*;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;
import com.citadelles.models.touslespersonnages.Architecte;

public class TestArchitecte {

    public static void main(String[] args) {
        TestArchitecte test = new TestArchitecte();
        test.test1();
        test.test2();
    }

    public void test1() {
        System.out.println("TEST DU CONSTRUCTEUR");
        Architecte architecte = new Architecte();
        try {
            Test.test(architecte.getNom().equals("Architecte"), "test du nom du personnage");

            Test.test(architecte.getRang() == 6, "test du rang du personnage");
            Test.test(architecte.getCaracteristiques().equals(Caracteristiques.ARCHITECTE),
                    "test des caract�ristiques du personnage");
            Test.test(architecte.getJoueur() == null, "test de l'initialisation de la variable \"joueur\"");
            Test.test(architecte.isAssassine() == false, "test de l'initialisation de la variable \"assassine\"");
            Test.test(architecte.isVole() == false, "test de l'initialisation de la variable \"vole\"");
        } catch (NullPointerException n) {
            System.out.println("Architecte n'a pas d'attributs");
        }
    }

    public void test2() {
        System.out.println("TEST DE L'UTILISATION DU POUVOIR");
        // on cr�e un plateau et on ajoute des cartes Quartier � la pioche:
        PlateauDeJeu plateau = new PlateauDeJeu();
        Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1);
        Quartier quartier2 = new Quartier(TYPE_QUARTIERS.MILITAIRE, "prison", 2);
        Quartier quartier3 = new Quartier(TYPE_QUARTIERS.NOBLE, "palais", 5);
        Pioche pioche = plateau.getPioche();
        pioche.ajouter(quartier1);
        pioche.ajouter(quartier2);
        pioche.ajouter(quartier3);
        // on ajoute le personnage au plateau:
        Architecte architecte = new Architecte();
        plateau.ajouterPersonnage(architecte);
        // on ajoute le joueur au plateau:
        Joueur joueur = new Joueur("Billy");
        plateau.ajouterJoueur(joueur);
        architecte.setJoueur(joueur);

        Test.test(architecte.getJoueur().nbrQuartiersDansMain() == 0,
                "test du nombre de cartes dans la main avant l'utilisation du pouvoir");
        architecte.utiliserPouvoir();
        Test.test(architecte.getJoueur().nbrQuartiersDansMain() == 2,
                "test du nombre de cartes dans la main apr�s l'utilisation du pouvoir");

    }
}
