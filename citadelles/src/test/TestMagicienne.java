package test;

import com.citadelles.models.Joueur;
import com.citadelles.models.Pioche;
import com.citadelles.models.PlateauDeJeu;
import com.citadelles.models.Quartier;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;
import com.citadelles.models.touslespersonnages.Assassin;
import com.citadelles.models.touslespersonnages.Magicienne;
import com.citadelles.models.touslespersonnages.Roi;

import java.util.ArrayList;

public class TestMagicienne {
    public static void main(String[] args) {
        TestMagicienne test = new TestMagicienne();
        test.test1();
        test.test2();
    }

    public void test1() {
        System.out.println("TEST DU CONSTRUCTEUR");
        PlateauDeJeu plateau = new PlateauDeJeu();
        Roi roi = new Roi();
        plateau.ajouterPersonnage(roi);
        Assassin assassin = new Assassin();
        plateau.ajouterPersonnage(assassin);
        Magicienne magicienne = new Magicienne();
        plateau.ajouterPersonnage(magicienne);
        Test.test(plateau.getNombrePersonnages() == 3, "nombre de personnages");
        Test.test(plateau.getPersonnage(2) == magicienne,
                "r�cup�ration du personnage de la magicienne");
        Test.test(plateau.getPersonnage(2).getRang() == 3,
                "rang de la magicienne");

    }

    public void test2() {
        System.out.println("TEST DU POUVOIR DE LA MAGICIENNE");
        PlateauDeJeu plateau = new PlateauDeJeu();

        // cr�ation de quatre personnages
        Roi roi = new Roi();
        plateau.ajouterPersonnage(roi);
        Assassin assassin = new Assassin();
        plateau.ajouterPersonnage(assassin);
        Magicienne magicienne = new Magicienne();
        plateau.ajouterPersonnage(magicienne);

        // cr�ation de trois joueurs
        Joueur joueur1 = new Joueur("Milou");
        plateau.ajouterJoueur(joueur1);
        Joueur joueur2 = new Joueur("Billy");
        plateau.ajouterJoueur(joueur2);
        Joueur joueur3 = new Joueur("Belle");
        plateau.ajouterJoueur(joueur3);

        // on associe les personnages aux joueurs
        roi.setJoueur(joueur1);
        assassin.setJoueur(joueur2);
        magicienne.setJoueur(joueur3);

        // cr�ation d'une pioche:
        Pioche pioche = plateau.getPioche();
        Quartier q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.MILITAIRE, "prison", 2);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.NOBLE, "palais", 5);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.COMMERCANT, "taverne", 1);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.COMMERCANT, "�choppe", 2);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.MERVEILLE, "basilique", 4, "A la fin de la partie, ...");
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "cath�drale", 5);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.MILITAIRE, "caserne", 3);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.NOBLE, "manoir", 3);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.COMMERCANT, "h�tel de ville", 15);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.NOBLE, "biblioth�que", 6, "Si vous choisissez...");
        pioche.ajouter(q);
        pioche.melanger();

        // on distribue les cartes aux joueurs:
        joueur1.ajouterQuartierDansMain(pioche.piocher());
        joueur1.ajouterQuartierDansMain(pioche.piocher());
        joueur1.ajouterQuartierDansMain(pioche.piocher());
        joueur2.ajouterQuartierDansMain(pioche.piocher());
        joueur2.ajouterQuartierDansMain(pioche.piocher());
        joueur2.ajouterQuartierDansMain(pioche.piocher());
        joueur3.ajouterQuartierDansMain(pioche.piocher());
        joueur3.ajouterQuartierDansMain(pioche.piocher());

        // on affiche la main de chaque joueur:
        System.out.print("Main du Roi (" + roi.getJoueur().getNom() + "): ");
        ArrayList<Quartier> mainRoi = roi.getJoueur().getMain();
        for (int i = 0; i < mainRoi.size(); i++)
            System.out.print(mainRoi.get(i).getNom() + ", ");
        System.out.println("");
        System.out.print("Main de l'assassin (" + assassin.getJoueur().getNom() + "): ");
        ArrayList<Quartier> mainAssassin = assassin.getJoueur().getMain();
        for (int i = 0; i < mainAssassin.size(); i++)
            System.out.print(mainAssassin.get(i).getNom() + ", ");
        System.out.println("");
        System.out.print("Main de la magicienne (" + magicienne.getJoueur().getNom() + "): ");
        ArrayList<Quartier> mainMagicienne = magicienne.getJoueur().getMain();
        for (int i = 0; i < mainMagicienne.size(); i++)
            System.out.print(mainMagicienne.get(i).getNom() + ", ");
        System.out.println("");

        // on r�cup�re la taille de la pioche:
        int taillePiocheAvantPouvoir = pioche.nombreElements();

        // utiliser le pouvoir de la magicienne :
        magicienne.utiliserPouvoir();

        // on r�affiche la main de chaque joueur:
        System.out.print("Main du Roi (" + roi.getJoueur().getNom() + "): ");
        for (int i = 0; i < mainRoi.size(); i++)
            System.out.print(mainRoi.get(i).getNom() + ", ");
        System.out.println("");
        System.out.print("Main de l'assassin (" + assassin.getJoueur().getNom() + "): ");
        for (int i = 0; i < mainAssassin.size(); i++)
            System.out.print(mainAssassin.get(i).getNom() + ", ");
        System.out.println("");
        System.out.print("Main de la magicienne (" + magicienne.getJoueur().getNom() + "): ");
        for (int i = 0; i < mainMagicienne.size(); i++)
            System.out.print(mainMagicienne.get(i).getNom() + ", ");
        System.out.println("");

        // on v�rifie que la taille de la pioche n'a pas chang�:
        Test.test(taillePiocheAvantPouvoir == pioche.nombreElements(),
                "taille inchang�e de la pioche");
    }

}
