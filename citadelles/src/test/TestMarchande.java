package test;

import com.citadelles.models.Caracteristiques;
import com.citadelles.models.Joueur;
import com.citadelles.models.Quartier;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;
import com.citadelles.models.touslespersonnages.Marchande;

public class TestMarchande {

	public static void main(String[] args) {
		TestMarchande test = new TestMarchande();
		test.test1();
		test.test2();
	}

	public void test1() {
		System.out.println("TEST DU CONSTRUCTEUR");
		Marchande Marchande = new Marchande();
		Test.test(Marchande.getNom().equals("Marchande"), "test du nom du personnage");
		Test.test(Marchande.getRang() == 6, "test du rang du personnage");
		Test.test(Marchande.getCaracteristiques().equals(Caracteristiques.MARCHANDE),
				"test des caract�ristiques du personnage");
		Test.test(Marchande.getJoueur() == null, "test de l'initialisation de la variable \"joueur\"");
		Test.test(Marchande.isAssassine() == false, "test de l'initialisation de la variable \"assassine\"");
		Test.test(Marchande.isVole() == false, "test de l'initialisation de la variable \"vole\"");
	}

	public void test2() {
		System.out.println("TEST DE LA PERCEPTION DE RESSOURCES SPECIFIQUES ET DE L'UTILISATEUR DU POUVOIR");
		Joueur joueur = new Joueur("Billy");
		Marchande marchande = new Marchande();
		Quartier quartier1 = new Quartier(TYPE_QUARTIERS.COMMERCANT, "taverne", 1);
		Quartier quartier2 = new Quartier(TYPE_QUARTIERS.MILITAIRE, "prison", 2);
		Quartier quartier3 = new Quartier(TYPE_QUARTIERS.COMMERCANT, "�choppe", 2);
		marchande.setJoueur(joueur);
		marchande.ajouterPieces();
		Test.test(marchande.getJoueur().getTresor() == 2,
				"test du nombre de pi�ces d'or avant perception");
		marchande.construire(quartier1);
		marchande.construire(quartier2);
		marchande.construire(quartier3);
		marchande.percevoirRessourcesSpecifiques();
		Test.test(marchande.getJoueur().getTresor() == 4,
				"test du nombre de pi�ces d'or apr�s perception de ressources sp�cifiques avec 2 quartiers commer�ants");
		marchande.utiliserPouvoir();
		Test.test(marchande.getJoueur().getTresor() == 5,
				"test du nombre de pi�ces d'or apr�s utilisation du pouvoir");

	}
}
