package test;

import com.citadelles.models.Caracteristiques;
import com.citadelles.models.Pioche;
import com.citadelles.models.Quartier;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;

public class TestPioche {
    public static void main(String[] args) {
        TestPioche testPioche = new TestPioche();
        testPioche.test1();
        testPioche.test2();
        testPioche.test3();
        testPioche.test4();
    }

    public void test1() {
        System.out.println("TEST DU CONSTRUCTEUR DE LA PIOCHE");
        Pioche pioche = new Pioche();
        Test.test(pioche.nombreElements() == 0, "taille de la pioche");
    }

    public void test2() {
        System.out.println("TEST DE L'AJOUT D'UN QUARTIER");
        Pioche pioche = new Pioche();
        Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
        Quartier quartier2 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
        Quartier quartier3 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
        pioche.ajouter(quartier1);
        pioche.ajouter(quartier2);
        pioche.ajouter(quartier3);
        Test.test(pioche.nombreElements() == 3, "taille de la pioche");
    }

    public void test3() {
        System.out.println("TEST DU RETRAIT D'UN QUARTIER");
        Pioche pioche = new Pioche();
        Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
        Quartier quartier2 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
        Quartier quartier3 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
        Quartier q;

        q = pioche.piocher();
        Test.test(q == null, "test d'un retrait dans une pioche vide");

        pioche.ajouter(quartier1);
        pioche.ajouter(quartier2);
        pioche.ajouter(quartier3);
        q = pioche.piocher();
        Test.test(pioche.nombreElements() == 2 && q == quartier1,
                "test d'un retrait dans une pioche compos�e de trois cartes");
        q = pioche.piocher();
        Test.test(pioche.nombreElements() == 1 && q == quartier2,
                "test d'un retrait dans une pioche compos�e de deux cartes");
        q = pioche.piocher();
        Test.test(pioche.nombreElements() == 0 && q == quartier3,
                "test d'un retrait dans une pioche compos�e d'une seule carte");
    }

    public void test4() {
        System.out.println("TEST DU MELANGE DE LA PIOCHE");
        Pioche pioche = new Pioche();
        Quartier q;
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.MERVEILLE, "taverne", 1, Caracteristiques.VOLEUR);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
        pioche.ajouter(q);
        q = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
        pioche.ajouter(q);

        pioche.melanger();
        Test.test(pioche.nombreElements() == 10, "taille de la pioche apr�s m�lange");
        System.out.println("Affichage de la pioche apr�s m�lange : ");
        for (int i = pioche.nombreElements(); i > 1; i--) {
            q = pioche.piocher();
            System.out.println("- " + q.getNom());
        }
    }
}
