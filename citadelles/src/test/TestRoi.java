package test;

import com.citadelles.models.Caracteristiques;
import com.citadelles.models.Joueur;
import com.citadelles.models.Quartier;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;
import com.citadelles.models.touslespersonnages.Roi;

public class TestRoi {

	public static void main(String[] args) {
		TestRoi testRoi = new TestRoi();
		testRoi.test1();
		testRoi.test2();
		testRoi.test3();
		testRoi.test4();
		testRoi.test5();
		testRoi.test6();
		testRoi.test7();
		testRoi.test8();
		testRoi.test9();
		testRoi.test10();
	}

	public void test1() {
		System.out.println("TEST DU CONSTRUCTEUR");
		Roi roi = new Roi();
		try {
			Test.test(roi.getNom().equals("Roi"), "test du nom du personnage Roi");
			Test.test(roi.getRang() == 4, "test du rang du personnage Roi");
			Test.test(roi.getCaracteristiques().equals(Caracteristiques.ROI),
					"test des caract�ristiques du personnage Roi");
			Test.test(roi.getJoueur() == null, "test de l'initialisation de la variable \"joueur\"");
			Test.test(roi.isAssassine() == false, "test de l'initialisation de la variable \"assassine\"");
			Test.test(roi.isVole() == false, "test de l'initialisation de la variable \"vole\"");
		} catch (Exception e) {
			System.out.println("roi est initialisé sans attribut!");
		}
	}

	public void test2() {
		System.out.println("TEST DE L'ATTRIBUTION D'UN JOUEUR");
		Joueur joueur = new Joueur("Billy");
		Roi roi = new Roi();
		roi.setJoueur(joueur);
		Test.test(roi.getJoueur() == joueur, "test de l'attribution de la variable \"joueur\"");
		Test.test(roi.getJoueur().getNom().equals("Billy"), "test du nom de joueur attribu�");
	}

	public void test3() {
		System.out.println("TEST DE L'ASSASSINAT DU PERSONNAGE");
		Roi roi = new Roi();
		roi.setAssassine(true);
		Test.test(roi.isAssassine() == true, "test de l'assassinat");
	}

	public void test4() {
		System.out.println("TEST DU VOL DU PERSONNAGE");
		Roi roi = new Roi();
		roi.setVole(true);
		Test.test(roi.isVole() == true, "test du vol");
	}

	public void test5() {
		System.out.println("TEST DE LA PERCEPTION DE PIECES D'OR");
		Joueur joueur = new Joueur("Billy");
		Roi roi = new Roi();
		roi.ajouterPieces();
		Test.test(roi.getJoueur() == null, "test alors que le joueur n'est pas attribu�");
		roi.setJoueur(joueur);
		roi.ajouterPieces();
		Test.test(roi.getJoueur().getTresor() == 2, "v�&rification du nombre de pi�ces d'or");
	}

	public void test6() {
		System.out.println("TEST DE L'AJOUT DE QUARTIER DANS LA MAIN DU JOUEUR");
		Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
		Quartier quartier2 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
		Quartier quartier3 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
		Joueur joueur = new Joueur("Billy");
		Roi roi = new Roi();
		roi.ajouterQuartier(quartier1);
		Test.test(roi.getJoueur() == null, "test alors que le joueur n'est pas attribu�");
		roi.setJoueur(joueur);
		roi.ajouterQuartier(quartier1);
		roi.ajouterQuartier(quartier2);
		roi.ajouterQuartier(quartier3);
		Test.test(roi.getJoueur().nbrQuartiersDansMain() == 3, "test du nombre de quartiers apr�s ajout");
	}

	public void test7() {
		System.out.println("TEST DE LA CONSTRUCTION D'UN QUARTIER DANS LA CITE DU JOUEUR");
		Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
		Quartier quartier2 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
		Quartier quartier3 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
		Joueur joueur = new Joueur("Billy");
		Roi roi = new Roi();
		roi.construire(quartier1);
		Test.test(roi.getJoueur() == null,
				"test alors que le joueur n'est pas attribu�");
		roi.setJoueur(joueur);
		roi.construire(quartier1);
		roi.construire(quartier2);
		roi.construire(quartier3);
		Test.test(roi.getJoueur().nbrQuartiersDansCite() == 3,
				"test du nombre de quartiers apr�s construction" + roi.getJoueur().nbrQuartiersDansCite());
		Test.test(roi.getJoueur().quartierPresentDansCite("prison"),
				"test de la pr�sence de la prison dans la cit�");
	}

	public void test8() {
		System.out.println("TEST DE LA PERCEPTION DE RESSOURCES SPECIFIQUES");
		Joueur joueur = new Joueur("Billy");
		Roi roi = new Roi();
		Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
		Quartier quartier2 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
		Quartier quartier3 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
		try {
			roi.percevoirRessourcesSpecifiques();
		} catch (Exception e) {
			System.out.println("Aucun joueur attribué à roi.");
		}
		Test.test(roi.getJoueur() == null,
				"test alors que le joueur n'est pas attribu�");
		roi.setJoueur(joueur);
		roi.ajouterPieces();
		Test.test(roi.getJoueur().getTresor() == 2,
				"test du nombre de pi�ces d'or avant perception");
		roi.percevoirRessourcesSpecifiques();
		Test.test(roi.getJoueur().getTresor() == 2,
				"test alors qu'il n'y a pas de quartiers nobles");
		roi.construire(quartier1);
		roi.construire(quartier2);
		roi.construire(quartier3);
		roi.percevoirRessourcesSpecifiques();
		Test.test(roi.getJoueur().getTresor() == 3,
				"test du nombre de pi�ces d'or apr�s perception de ressources sp�cifiques avec 1 quartier noble");
	}

	public void test9() {
		System.out.println("TEST DE L'UTILISATION DU POUVOIR DU ROI");
		Joueur joueur = new Joueur("Billy");
		Roi roi = new Roi();
		try {
			roi.utiliserPouvoir();
		} catch (Exception e) {
			System.out.println("Aucun joueur attribué au roi!");
		}
		Test.test(roi.getJoueur() == null,
				"test alors que le joueur n'est pas attribu�");
		roi.setJoueur(joueur);
		Test.test(roi.getJoueur().isPossedeCouronne() == false, "test avant utilisation");
		roi.utiliserPouvoir();
		Test.test(roi.getJoueur().isPossedeCouronne() == true, "test apr�s utilisation");
	}

	public void test10() {
		System.out.println("TEST DE LA REINITIALISATION");
		Joueur joueur = new Joueur("Billy");
		Roi roi = new Roi();
		roi.setJoueur(joueur);
		roi.setAssassine(false);
		roi.setVole(false);
		roi.reinitialiser();
		Test.test(roi.getJoueur() == null, "test du joueur non attribu�");
		Test.test(roi.isAssassine() == false, "test de l'assassinat du personnage");
		Test.test(roi.isVole() == false, "test du vol du personnage");
	}
}
