package test;

import com.citadelles.models.Caracteristiques;
import com.citadelles.models.Quartier;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;

public class TestQuartier {
	public static void main(String[] args) {

		TestQuartier testQuartier = new TestQuartier();

		testQuartier.test1();
		testQuartier.test2();
		testQuartier.test3();
		testQuartier.test4();
		testQuartier.test5();
		testQuartier.test6();
		testQuartier.test7();
	}

	public void test1() {
		System.out.println("TEST DU CONSTRUCTEUR VIDE");
		Quartier quartier = new Quartier();
		try {
			Test.test(quartier.getNom().equals(""), "test du nom du quartier");
			Test.test(quartier.getType_quartiers().equals(""), "test du type du quartier");
			Test.test(quartier.getCoutConstruction() == 0, "test du cout du quartier");
			Test.test(quartier.getCaracteristiques().equals(""), "test des caracteristiques du quartier");
		} catch (Exception e) {
			System.out.println("quartier est initialisé sans attribut!");
		}
	}

	public void test2() {
		System.out.println("TEST POUR UN TEMPLE (RELIGIEUX");
		Quartier quartier = new Quartier(TYPE_QUARTIERS.MERVEILLE, "temple", 1, Caracteristiques.ABBE);
		Test.test(quartier.getNom().equals("temple"), "test du nom du quartier");
		Test.test(quartier.getType_quartiers().equals(TYPE_QUARTIERS.RELIGIEUX), "test du type du quartier");
		Test.test(quartier.getCoutConstruction() == 1, "test du cout du quartier");
		Test.test(quartier.getCaracteristiques().equals(""), "test des caracteristiques du quartier");
	}

	public void test3() {
		System.out.println("TEST POUR UNE PRISON (MILITAIRE)");
		Quartier quartier = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.BAILLI);
		Test.test(quartier.getNom().equals("prison"), "test du nom du quartier");
		Test.test(quartier.getType_quartiers().equals(TYPE_QUARTIERS.MILITAIRE), "test du type du quartier");
		Test.test(quartier.getCoutConstruction() == 2, "test du cout du quartier");
		Test.test(quartier.getCaracteristiques().equals(""), "test des caracteristiques du quartier");
	}

	public void test4() {
		System.out.println("TEST POUR UN PALAIS (NOBLE)");
		Quartier quartier = new Quartier(TYPE_QUARTIERS.COMMERCANT, "palais", 5, Caracteristiques.ALCHIMISTE);
		Test.test(quartier.getNom().equals("palais"), "test du nom du quartier");
		Test.test(quartier.getType_quartiers().equals(TYPE_QUARTIERS.NOBLE), "test du type du quartier");
		Test.test(quartier.getCoutConstruction() == 5, "test du cout du quartier");
		Test.test(quartier.getCaracteristiques().equals(""), "test des caracteristiques du quartier");
	}

	public void test5() {
		System.out.println("TEST POUR UN MARCHE (COMMERCANT)");
		Quartier quartier = new Quartier(TYPE_QUARTIERS.NOBLE, "march�", 2, Caracteristiques.BAILLI);
		Test.test(quartier.getNom().equals("march�"), "test du nom du quartier");
		Test.test(quartier.getType_quartiers().equals(TYPE_QUARTIERS.COMMERCANT), "test du type du quartier");
		Test.test(quartier.getCoutConstruction() == 2, "test du cout du quartier");
		Test.test(quartier.getCaracteristiques().equals(""), "test des caracteristiques du quartier");
	}

	public void test6() {
		String caracteristiques = "Le Donjon ne peut �tre affect� par les pouvoirs des personnages de rang 8";
		System.out.println("TEST POUR LA MERVEILLE DONJON");
		Quartier quartier = new Quartier(TYPE_QUARTIERS.NOBLE, "Donjon", 3, Caracteristiques.BAILLI);
		Test.test(quartier.getNom().equals("Donjon"), "test du nom de la merveille");
		Test.test(quartier.getType_quartiers().equals(TYPE_QUARTIERS.MERVEILLE), "test du type de la merveille");
		Test.test(quartier.getCoutConstruction() == 3, "test du cout de la merveille");
		Test.test(quartier.getCaracteristiques().equals(caracteristiques), "test des caracteristiques de la merveille");
	}

	public void test7() {
		System.out.println("TEST DES ACCESSEURS EN ECRITURE");
		Quartier quartier = new Quartier();
		quartier.setNom("Basilique");
		Test.test(quartier.getNom().equals("Basilique"), "test du changement de nom du quartier");
		quartier.setCoutConstruction(7);
		Test.test(quartier.getCoutConstruction() == 0, "test d'un mauvais changement de cout");
		quartier.setCoutConstruction(-1);
		Test.test(quartier.getCoutConstruction() == 0, "test d'un deuxieme mauvais changement de cout");
		quartier.setCoutConstruction(4);
		Test.test(quartier.getCoutConstruction() == 4, "test d'un bon changement de cout");
		quartier.setType_quartiers(TYPE_QUARTIERS.MERVEILLE);
		Test.test(quartier.getType_quartiers().equals(""), "test d'un mauvais changement du type");
		quartier.setType_quartiers(TYPE_QUARTIERS.MERVEILLE);
		Test.test(quartier.getType_quartiers().equals("MERVEILLE"), "test d'un bon changement du type");
		quartier.setCaracteristiques("A la fin de la partie...");
		Test.test(quartier.getCaracteristiques().equals("A la fin de la partie..."),
				"test du changement des caracteristiques");
	}
}
