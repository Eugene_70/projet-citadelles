package test;

import com.citadelles.models.Caracteristiques;
import com.citadelles.models.Joueur;
import com.citadelles.models.Quartier;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;
import com.citadelles.models.touslespersonnages.Eveque;

public class TestEveque {

    public static void main(String[] args) {
        TestEveque test = new TestEveque();
        test.test1();
        test.test2();
    }

    public void test1() {
        System.out.println("TEST DU CONSTRUCTEUR");
        Eveque eveque = new Eveque();
        try {
            Test.test(eveque.getNom().equals("Eveque"), "test du nom du personnage");
            Test.test(eveque.getRang() == 5, "test du rang du personnage");
            Test.test(eveque.getCaracteristiques().equals(Caracteristiques.EVEQUE),
                    "test des caract�ristiques du personnage");
            Test.test(eveque.getJoueur() == null, "test de l'initialisation de la variable \"joueur\"");
            Test.test(eveque.isAssassine() == false, "test de l'initialisation de la variable \"assassine\"");
            Test.test(eveque.isVole() == false, "test de l'initialisation de la variable \"vole\"");
        } catch (NullPointerException n) {
            System.out.println("eveque n'a pas d'attributs");
        }
    }

    public void test2() {
        System.out.println("TEST DE LA PERCEPTION DE RESSOURCES SPECIFIQUES");
        Joueur joueur = new Joueur("Billy");
        Eveque eveque = new Eveque();
        Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1);
        Quartier quartier2 = new Quartier(TYPE_QUARTIERS.MILITAIRE, "prison", 2);
        Quartier quartier3 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "�glise", 2);
        eveque.setJoueur(joueur);
        eveque.ajouterPieces();
        Test.test(eveque.getJoueur().getTresor() == 2,
                "test du nombre de pi�ces d'or avant perception");
        eveque.construire(quartier1);
        eveque.construire(quartier2);
        eveque.construire(quartier3);
        eveque.percevoirRessourcesSpecifiques();
        Test.test(eveque.getJoueur().getTresor() == 4,
                "test du nombre de pi�ces d'or apr�s perception de ressources sp�cifiques avec 2 quartiers religieux");
    }
}