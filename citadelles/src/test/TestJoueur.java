package test;

import com.citadelles.models.Caracteristiques;
import com.citadelles.models.Joueur;
import com.citadelles.models.Quartier;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;

public class TestJoueur {
    public static void main(String[] args) {
        TestJoueur testJoueur = new TestJoueur();
        testJoueur.test1();
        testJoueur.test2();
        testJoueur.test3();
        testJoueur.test4();
        testJoueur.test5();
    }

    public void test1() {
        System.out.println("TEST DU CONSTRUCTEUR");
        Joueur joueur = new Joueur("Billy");
        Test.test(joueur.getNom().equals("Billy"), "test du nom du joueur");
        Test.test(joueur.getTresor() == 0, "test du tr�sor initial du joueur");
        Test.test(joueur.getNbQuartiers() == 0,
                "test de nombre de quartiers dans la cite");
        Test.test(joueur.nbrQuartiersDansMain() == 0,
                "test du nombre de quartiers dans la main du joueur");
    }

    public void test2() {
        System.out.println("TEST DU TRESOR DU JOUEUR");
        Joueur joueur = new Joueur("Billy");
        joueur.ajouterPieces(2);
        joueur.ajouterPieces(1);
        Test.test(joueur.getTresor() == 3, "test de l'ajout d'un nombre de pi�ces");
        joueur.ajouterPieces(-2);
        Test.test(joueur.getTresor() == 3, "test d'ajout d'un nombre de pi�ces n�gatif");
        joueur.retirerPieces(-1);
        Test.test(joueur.getTresor() == 3, "test d'un retrait d'un nombre n�gatif de pi�ces");
        joueur.retirerPieces(4);
        Test.test(joueur.getTresor() == 3, "test de retrait d'un trop grand nombre de pi�ces");
        joueur.retirerPieces(2);
        joueur.retirerPieces(1);
        Test.test(joueur.getTresor() == 0, "test de retrait d'un nombre de pi�ces");
    }

    public void test3() {
        System.out.println("TEST DE LA CITE DU JOUEUR");
        Joueur joueur = new Joueur("Billy");
        Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
        Quartier quartier2 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
        Quartier quartier3 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
        joueur.ajouterQuartierDansCite(quartier1);
        joueur.ajouterQuartierDansCite(quartier2);
        joueur.ajouterQuartierDansCite(quartier3);
        Test.test(joueur.nbrQuartiersDansCite() == 3, "test de l'ajout de trois quartiers");
        Test.test(joueur.quartierPresentDansCite("temple"), "test de pr�sence d'un quartier");
        Quartier retour = joueur.retirerQuartierDansCite("prison");
        Test.test(joueur.nbrQuartiersDansCite() == 2 && retour == quartier2,
                "test de retrait d'un quartier");
        Test.test(!joueur.quartierPresentDansCite("march�"), "test de non pr�sence d'un quartier");
    }

    public void test4() {
        System.out.println("TEST DE LA MAIN DU JOUEUR");
        Joueur joueur = new Joueur("Billy");
        Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
        Quartier quartier2 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
        Quartier quartier3 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);

        joueur.ajouterQuartierDansMain(quartier1);
        joueur.ajouterQuartierDansMain(quartier2);
        joueur.ajouterQuartierDansMain(quartier3);
        Test.test(joueur.nbrQuartiersDansMain() == 3, "test de l'ajout de trois quartiers");
        Quartier retour = joueur.retirerQuartierDansMain();
        Test.test(joueur.nbrQuartiersDansMain() == 2 &&
                        (retour == quartier1 || retour == quartier2 || retour == quartier3),
                "test de retrait d'un quartier");
    }

    public void test5() {
        System.out.println("TEST DE LA REINITIALISATION DU JOUEUR");
        Joueur joueur = new Joueur("Billy");
        Quartier quartier1 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "temple", 1, Caracteristiques.ROI);
        Quartier quartier2 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "prison", 2, Caracteristiques.SORCIERE);
        Quartier quartier3 = new Quartier(TYPE_QUARTIERS.RELIGIEUX, "palais", 5, Caracteristiques.VOLEUR);
        joueur.ajouterQuartierDansMain(quartier1);
        joueur.ajouterQuartierDansMain(quartier2);
        joueur.ajouterQuartierDansCite(quartier3);
        joueur.reinitialiser();
        joueur.ajouterPieces(2);
        joueur.reinitialiser();
        Test.test(joueur.nbrQuartiersDansMain() == 0 && joueur.nbrQuartiersDansCite() == 0
                && joueur.getTresor() == 0, "test de la r�initialisation");
    }
}