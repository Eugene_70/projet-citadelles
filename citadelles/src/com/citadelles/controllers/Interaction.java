package com.citadelles.controllers;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Interaction {
    private static final Scanner sc = new Scanner(System.in);

    public static int lireUnEntier() {
        int i = 0;
        boolean continu = true;
        do {
            try {
                i = sc.nextInt();
                if (i >= 0) continu = false;
                else System.out.print("Veuillez rentrer un chiffre entier positif.");
            } catch (InputMismatchException e) {
                System.out.print("Veuillez rentrer un chiffre entier.");
                sc.next(); // passe l'entier pour �viter de boucler
            }
        } while (continu);
        return i;
    }

    /**
     * Renvoie un entier lu au clavier compris dans l'intervalle [borneMin, borneMax[
     */
    public static int lireUnEntier(int borneMin, int borneMax) {
        int i = 0;
        boolean continu = true;

        do {
            try {
                i = sc.nextInt();
                if (i < borneMin || i >= borneMax) {
                    System.out.println("Vous devez entrer un chiffre compris entre [ " + borneMin + " ; " + borneMax + " [ ");
                } else
                    continu = false;

            } catch (InputMismatchException e) {
                System.out.print("Veuillez rentrer un chiffre entier.");
            }

        } while (continu);

        return i;
    }

    /**
     * Lit les r�ponses "oui", "non", "o" ou "n" et renvoie un bool�en
     */
    public static boolean lireOuiOuNon() {

        boolean retour = false;
        boolean continu = true;
        String reponse = "";

        do {
            reponse = lireUneChaine().toLowerCase();

            if (reponse.equals("oui") || reponse.equals("o")) {
                retour = true;
                continu = false;
            } else if (reponse.equals("non") || reponse.equals("n")) {
                continu = false;
            } else {
                System.out.println("Reponses possibles : Oui(o) ou Non(n)");
            }
        } while (continu);

        return retour;
    }

    /**
     * Renvoie une cha�ne de caractère lue au clavier
     */
    public static String lireUneChaine() {
        String retour = "";

        retour = sc.next();

        return retour;
    }


}