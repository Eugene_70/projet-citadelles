package com.citadelles.models;

import lombok.Data;

@Data
public abstract class Personnage {

    protected String nom;
    protected int rang;
    protected String caracteristiques;
    protected Joueur joueur;
    protected boolean assassine;
    protected boolean vole;
    protected PlateauDeJeu plateauDeJeu;

    public Personnage() {
    }

    public Personnage(String nom, int rang, String caracteristiques) {
        this.nom = nom;
        this.rang = rang;
        this.caracteristiques = caracteristiques;
        this.joueur = null;
        this.assassine = false;
        this.vole = false;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
        this.joueur.monPersonnage = this;
    }

    public void ajouterPieces() {

        if (this.joueur == null || this.isAssassine())
            System.out.println("Aucun joueur associé au personnage ou personnage assassiné.");
        else
            this.joueur.setTresor(2);
    }

    public void ajouterQuartier(Quartier quartier) {

        if (this.joueur == null || this.isAssassine())
            System.out.println("Aucun joueur associé au personnage ou personnage assissiné.");
        else
            this.joueur.ajouterQuartierDansMain(quartier);
    }

    public void construire(Quartier quartier) {

        if (this.joueur == null || this.isAssassine())
            System.out.println("Aucun joueur associé au personnage ou personnage assaciné.");
        else
            this.joueur.ajouterQuartierDansCite(quartier);
    }

    // TODO :: certains personnages ne recoivent aucune ressorce specifique.
    //  Il faudra gerer le cas de chaque personnage
    public void percevoirRessourcesSpecifiques() {

        System.out.println("Aucune ressource specifique.");
    }

    public abstract void utiliserPouvoir();

    public abstract void utiliserPouvoirAvatar();

    public void reinitialiser() {

        if (this.joueur != null) this.joueur.monPersonnage = null;

        this.joueur = null;
        this.assassine = false;
        this.vole = false;
    }

}
