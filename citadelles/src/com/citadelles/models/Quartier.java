package com.citadelles.models;

import com.citadelles.models.enumerations.TYPE_QUARTIERS;
import lombok.Data;

@Data
public class Quartier {

    private TYPE_QUARTIERS type_quartiers;
    private String nom;
    private int coutConstruction;
    private String caracteristiques;

    public Quartier() {

    }

    public Quartier(TYPE_QUARTIERS type_quartiers, String nom, int coutConstruction) {
        this.type_quartiers = type_quartiers;
        this.nom = nom;
        this.coutConstruction = coutConstruction;
    }

    public Quartier(TYPE_QUARTIERS type_quartiers, String nom, int coutConstruction, String caracteristiques) {
        this.type_quartiers = type_quartiers;
        this.nom = nom;
        this.coutConstruction = coutConstruction;
        this.caracteristiques = caracteristiques;
    }

    public void setCoutConstruction(int coutConstruction) {
        if (coutConstruction < 1 || coutConstruction > 6)
            this.coutConstruction = 0;
        else
            this.coutConstruction = coutConstruction;
    }
}
