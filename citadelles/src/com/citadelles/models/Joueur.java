package com.citadelles.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.Random;

@Data
public class Joueur {

    private String nom;
    private int tresor;
    private Quartier[] cite;
    /**
     * Nombre de qartiers presents dans la cite
     */
    private int nbQuartiers;
    private ArrayList<Quartier> main;
    private boolean possedeCouronne;
    protected Personnage monPersonnage;

    public Joueur(String nom) {
        this.nom = nom;
        this.tresor = 0;
        this.nbQuartiers = 0;
        this.possedeCouronne = false;
        this.cite = new Quartier[8];
        this.main = new ArrayList<Quartier>();
        this.monPersonnage = null;
    }

    public void setTresor(int i) {
        this.tresor += i;
    }

    public int nbrQuartiersDansCite() {
        int nbrQuartier = 0;
        for (int i = 0; i < this.nbQuartiers; i++) {
            nbrQuartier++;
        }
        this.nbQuartiers = nbrQuartier;

        return this.nbQuartiers;
    }

    /**
     * Compte le nombre de fois que le quartier est present dans la cité
     *
     * @param quartier
     * @return
     */
    public int nbrFoisQuartiersDansCite(String quartier) {
        int nbrQuartier = 0;
        for (int i = 0; i < this.nbQuartiers; i++) {
            if (this.cite[i].getNom().equals(quartier))
                nbrQuartier++;
        }
        this.nbQuartiers = nbrQuartier;

        return this.nbQuartiers;
    }

    public int nbrQuartiersDansMain() {
        int nbrQuartier = 0;
        for (int i = 0; i < this.main.size(); i++) {
            nbrQuartier++;
        }
        return nbrQuartier;
    }

    public void ajouterPieces(int i) {
        if (i > 0)
            this.tresor += i;
        else
            System.out.println("Nombre de pieces a ajouter incorrect...");
    }

    public void retirerPieces(int i) {

        if (i > 0 && this.tresor >= i)
            this.tresor -= i;
        else
            System.out.println("Nombre de pieces a retirer incorrect...");
    }

    public void ajouterQuartierDansCite(Quartier quartier) {

        if (quartierPresentDansCite(quartier.getNom())) {
            System.out.println("Le quartier est present dans la citee.");
        } else if (quartier != null && this.nbQuartiers < 9) {
            this.cite[this.nbQuartiers] = quartier;
            this.nbQuartiers++;
        } else
            System.out.println("La cité est pleine.");
    }

    public Quartier retirerQuartierDansCite(String nomQuartier) {

        Quartier quartier;

        for (int i = 0; i < this.nbQuartiers; i++) {
            if (nomQuartier != null && nomQuartier.equals(this.cite[i].getNom())) {
                quartier = this.cite[i];
                // Pour retirer le quartier trouvé, on ecrase sa valeur
                // par les quartier a indice superieur
                for (int j = i + 1; j < this.nbQuartiers; j++) {
                    this.cite[j - 1] = this.cite[j];
                }
                this.cite[this.nbQuartiers - 1] = null;
                this.nbQuartiers--;

                return quartier; // Quartier retiré
            }
        }
        return null; // Aucun quartier retiré
    }

    public boolean quartierPresentDansCite(String nomQuartier) {

        for (int i = 0; i < this.nbQuartiers; i++) {
            if (nomQuartier != null && nomQuartier.equals(this.cite[i].getNom())) {
                return true;
            }
        }
        return false;
    }

    public void ajouterQuartierDansMain(Quartier quartier) {

        if (quartier != null)
            this.main.add(quartier);
        else
            System.out.println("quartier null...");

    }

    public Quartier retirerQuartierDansMain() {

        if (this.nbrQuartiersDansMain() <= 0)
            return null;

        Random random = new Random();
        int nbrAleatoire = random.nextInt(this.nbrQuartiersDansMain());

        return this.main.remove(nbrAleatoire);
    }

    /**
     * Remet à zero le trésor, vide la main et la cite
     */
    public void reinitialiser() {

        this.tresor = 0;
        // reinitialisation de la cite
        this.cite = new Quartier[8];
        this.nbQuartiers = 0;
        // reinitialisation de la main
        this.main = new ArrayList<Quartier>();
    }

}
