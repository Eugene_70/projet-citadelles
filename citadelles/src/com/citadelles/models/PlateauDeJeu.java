package com.citadelles.models;

import lombok.Data;

@Data
public class PlateauDeJeu {

    private Personnage[] listePersonnages;
    private Joueur[] listeJoueurs;
    private Pioche pioche;
    private int nombrePersonnages;
    private int nombreJoueurs;

    public PlateauDeJeu() {
        this.listeJoueurs = new Joueur[9];
        this.listePersonnages = new Personnage[9];
        this.pioche = new Pioche();
        this.nombreJoueurs = 0;
        this.nombrePersonnages = 0;
    }

    /**
     * Retourne le i-ème joueur
     *
     * @param i
     * @return
     */
    public Joueur getJoueur(int i) {
        return this.listeJoueurs[i];
    }

    /**
     * Retourne le i-ème Personnage
     *
     * @param i
     * @return
     */
    public Personnage getPersonnage(int i) {
        return this.listePersonnages[i];
    }

    public void ajouterJoueur(Joueur joueur) {

        for (int i = 0; i < this.listeJoueurs.length; i++) {
            if (joueur != null && this.listeJoueurs[i] == null) {
                this.listeJoueurs[i] = joueur;
                this.nombreJoueurs++;
                break;
            }
        }
    }

    public void ajouterPersonnage(Personnage personnage) {

        for (int i = 0; i < this.listePersonnages.length; i++) {
            if (personnage != null && this.listePersonnages[i] == null) {
                personnage.setPlateauDeJeu(this);
                this.listePersonnages[i] = personnage;
                this.nombrePersonnages++;
                break;
            }
        }
    }
}
