package com.citadelles.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;

@Data
public class Pioche {

    private ArrayList<Quartier> liste;

    public Pioche() {
        this.liste = new ArrayList<Quartier>();
    }

    /**
     * retire le premier element de la pioche avant de le renvoyer.
     * Noter que dans une pioche, le premier element est le dernier element de la liste.
     *
     * @return
     */
    public Quartier piocher() {

        if (this.liste.size() - 1 > 0)
            return this.liste.remove(this.liste.size() - 1);
        else return null;

    }

    /**
     * Ajoute une carte quartier au bas de la Pioche.
     * Noter que dans une pioche, le premier element est le dernier element de la liste..
     *
     * @param quartier
     */
    public void ajouter(Quartier quartier) {

        this.liste.add(0, quartier);
    }

    /**
     * Renvoie le nombre d'element de la pioche.
     *
     * @return
     */
    public int nombreElements() {
        return this.liste.size();
    }

    /**
     * Melange la Pioche
     */
    public void melanger() {
        Collections.shuffle(this.liste);
    }
}
