package com.citadelles.models.touslespersonnages;

import com.citadelles.controllers.Interaction;
import com.citadelles.models.Personnage;

import java.util.ArrayList;

public class Assassin extends Personnage {

    public Assassin() {
        super();
    }

    public Assassin(String nom, int rang, String caracteristiques) {
        super(nom, rang, caracteristiques);
    }

    @Override
    public void utiliserPouvoir() {
        int indicePersonnage = choisirPersonnageAAssassine();
        System.out.println("Voulez-vous vraiment l'assassiné ? (Oui/Non)");
        if (Interaction.lireOuiOuNon())
            this.plateauDeJeu.getPersonnage(indicePersonnage).setAssassine(true);
        else
            utiliserPouvoir();
    }

    @Override
    public void utiliserPouvoirAvatar() {

    }

    private int choisirPersonnageAAssassine() {

        ArrayList<Integer> listeIndices = indicesPersonnagesVivant();
        // permet de stocker les correspondances entre le numero du personnage et son indice dans la liste des personnages du plateau de jeu
        ArrayList<Integer> indicePourAssassina = new ArrayList<Integer>();

        System.out.println("Quel personnage voulez-vous assassiner ? ");

        for (int i = 0; i < listeIndices.size(); i++) {
            System.out.println(i + 1 + "- " + this.plateauDeJeu.getPersonnage(listeIndices.get(i)).getNom());
            indicePourAssassina.set(i + 1, listeIndices.get(i));
        }
        int choix = Interaction.lireUnEntier(1, listeIndices.size());

        return indicePourAssassina.get(choix);
    }

    /**
     * Renvoie les indices de tous les personnages non assassinés du jeu sauf l'assassin
     */
    private ArrayList<Integer> indicesPersonnagesVivant() {

        ArrayList<Integer> listeIndice = new ArrayList<Integer>();

        for (int i = 0; i < this.plateauDeJeu.getNombrePersonnages(); i++) {
            // S'il s'agit de l'assassin ou si le personage est deja assassiné on passe
            if (this.plateauDeJeu.getPersonnage(i).getNom().equals(this.nom)
                    || this.plateauDeJeu.getPersonnage(i).isAssassine())
                continue;
            else { // sinon on l'ajoute a la liste
                listeIndice.add(i);
            }
        }
        return listeIndice;
    }

}
