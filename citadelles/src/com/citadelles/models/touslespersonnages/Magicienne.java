package com.citadelles.models.touslespersonnages;

import com.citadelles.controllers.Interaction;
import com.citadelles.models.Personnage;
import com.citadelles.models.Quartier;

import java.util.ArrayList;

public class Magicienne extends Personnage {
    public Magicienne() {
        super();
    }

    public Magicienne(String nom, int rang, String caracteristiques) {
        super(nom, rang, caracteristiques);
    }

    @Override
    public void utiliserPouvoir() {

        System.out.println("Que voulez-vous faire ? ");
        System.out.println("1- Echanger toutes ses cartes avec celles d’un adversaire");
        System.out.println("2- Remplacer des cartes dans la pioche");
        System.out.println("3- Ne rien faire");

        int choix = Interaction.lireUnEntier(1, 4);

        switch (choix) {

            case 1: // Si l’utilisateur veut échanger toutes ses cartes avec celles d’un adversaire

                // en cas de main du magicien vide
                if (this.joueur.getMain().size() < 1) {
                    System.out.println("Vous n'avez aucune carte.");
                    return;
                }

                int indicePersonnage = choisirPersonnageADefausser();
                System.out.println("Voulez-vous vraiment le defausser ? (Oui/Non)");

                if (Interaction.lireOuiOuNon()) {

                    //copier les mains
                    ArrayList<Quartier> copieMainMagicienne = new ArrayList<Quartier>(this.joueur.getMain());
                    ArrayList<Quartier> copieMainAutrePerso = new ArrayList<Quartier>(this.plateauDeJeu.getPersonnage(indicePersonnage).getJoueur().getMain());

                    // vider les mains
                    do {
                        this.joueur.retirerQuartierDansMain();
                    } while (this.joueur.nbrQuartiersDansMain() > 0);
                    do {
                        this.plateauDeJeu.getPersonnage(indicePersonnage).getJoueur().retirerQuartierDansMain();
                    } while (this.plateauDeJeu.getPersonnage(indicePersonnage).getJoueur().nbrQuartiersDansMain() > 0);

                    // echange des cartes
                    for (Quartier quartier : copieMainAutrePerso) this.joueur.ajouterQuartierDansMain(quartier);
                    for (Quartier quartier : copieMainMagicienne)
                        this.plateauDeJeu.getPersonnage(indicePersonnage).getJoueur().ajouterQuartierDansMain(quartier);

                } else
                    utiliserPouvoir();

                break;

            case 2: // Demander à l’utilisateur le nombre de cartes nb qu’il veut remplacer
                System.out.println("Combien de cartes voulez-vous remplacer ? ");
                int nbr = Interaction.lireUnEntier(0, this.joueur.nbrQuartiersDansMain());

                if (nbr <= 0) { //si nb vaut 0, ne rien faire

                } else if (nbr == this.joueur.nbrQuartiersDansMain()) { // Si nb correspond au nombre de cartes de la main de la magicienne
                    //copier la main
                    ArrayList<Quartier> copieMainMagicienne = new ArrayList<Quartier>(this.joueur.getMain());

                    // vider la main
                    do {
                        this.joueur.retirerQuartierDansMain();
                    } while (this.joueur.nbrQuartiersDansMain() > 0);

                    // echange des cartes
                    for (int i = 0; i < nbr; i++)
                        this.joueur.ajouterQuartierDansMain(this.plateauDeJeu.getPioche().getListe().get(i));
                    for (Quartier quartier : copieMainMagicienne) this.plateauDeJeu.getPioche().ajouter(quartier);

                } else { // il choisit un certain nombre de cartes a echanger dans la pioche
                    //copier la main
                    ArrayList<Quartier> copieMainMagicienne = new ArrayList<Quartier>(this.joueur.getMain());
                    // Affichage des cartes de la copie
                    System.out.println("Voici les cartes de votre main :");
                    for (int i = 0; i < copieMainMagicienne.size(); i++) {
                        System.out.println(i + 1 + "- " + copieMainMagicienne.get(i).getNom() + " - Type: "
                                + copieMainMagicienne.get(i).getType_quartiers()
                                + " - Pieces: " + copieMainMagicienne.get(i).getCoutConstruction());
                    }

                    // choisir a chaque fois la carte a remplacer
                    for (int i = 0; i < nbr; i++) {
                        System.out.println("Quel est le numéro de la carte que vous voulez retirer ? ");
                        int num = Interaction.lireUnEntier(1, this.joueur.nbrQuartiersDansMain());
                        // Supprimer la carte de la copie et l'aouter dans la pioche
                        this.plateauDeJeu.getPioche().ajouter(copieMainMagicienne.remove(num - 1));
                    }

                    // Ajouter nbr carte a la copie
                    for (int i = 0; i < nbr; i++) {
                        copieMainMagicienne.add(this.plateauDeJeu.getPioche().piocher());
                    }
                    // vider la main
                    do {
                        this.joueur.retirerQuartierDansMain();
                    } while (this.joueur.nbrQuartiersDansMain() > 0);

                    // Ajouter toutes les cartes de la copie dans la main (originale)
                    for (Quartier quartier : copieMainMagicienne) this.joueur.ajouterQuartierDansMain(quartier);
                }

                break;

            default: // Si l’utilisateur n’a pas de cartes dans sa main
                break;
        }
    }

    @Override
    public void utiliserPouvoirAvatar() {

    }

    private int choisirPersonnageADefausser() {

        ArrayList<Integer> listeIndices = indicesPersonnages();
        // permet de stocker les correspondances entre le numero du personnage et son indice dans la liste des personnages du plateau de jeu
        ArrayList<Integer> indicePourVol = new ArrayList<Integer>();

        System.out.println("Quel personnage voulez-vous defausser ? ");

        for (int i = 0; i < listeIndices.size(); i++) {
            System.out.println(i + 1 + "- " + this.plateauDeJeu.getPersonnage(listeIndices.get(i)).getNom() +
                    " -- " + this.plateauDeJeu.getPersonnage(listeIndices.get(i)).getJoueur().nbrQuartiersDansMain() + " cartes");
            indicePourVol.set(i + 1, listeIndices.get(i));
        }
        int choix = Interaction.lireUnEntier(1, listeIndices.size());

        return indicePourVol.get(choix);
    }

    /**
     * Renvoie les indices de tous les personnages du jeu sauf la magicienne
     */
    private ArrayList<Integer> indicesPersonnages() {

        ArrayList<Integer> listeIndice = new ArrayList<Integer>();

        for (int i = 0; i < this.plateauDeJeu.getNombrePersonnages(); i++) {
            // S'il s'agit de la magicienne
            if (this.plateauDeJeu.getPersonnage(i).getNom().equals(this.nom))
                continue;
            else { // sinon on l'ajoute a la liste
                listeIndice.add(i);
            }
        }
        return listeIndice;
    }
}
