package com.citadelles.models.touslespersonnages;

import com.citadelles.controllers.Interaction;
import com.citadelles.models.Personnage;

import java.util.ArrayList;

public class Voleur extends Personnage {

    public Voleur() {
        super();
    }

    public Voleur(String nom, int rang, String caracteristiques) {
        super(nom, rang, caracteristiques);
    }

    @Override
    public void utiliserPouvoir() {
        int indicePersonnage = choisirPersonnageAVoler();
        System.out.println("Voulez-vous vraiment le voler ? (Oui/Non)");
        if (Interaction.lireOuiOuNon())
            this.plateauDeJeu.getPersonnage(indicePersonnage).setVole(true);
        else
            utiliserPouvoir();
    }

    @Override
    public void utiliserPouvoirAvatar() {

    }

    private int choisirPersonnageAVoler() {

        ArrayList<Integer> listeIndices = indicesPersonnages();
        // permet de stocker les correspondances entre le numero du personnage et son indice dans la liste des personnages du plateau de jeu
        ArrayList<Integer> indicePourVol = new ArrayList<Integer>();

        System.out.println("Quel personnage voulez-vous voler ? ");

        for (int i = 0; i < listeIndices.size(); i++) {
            System.out.println(i + 1 + "- " + this.plateauDeJeu.getPersonnage(listeIndices.get(i)).getNom());
            indicePourVol.set(i + 1, listeIndices.get(i));
        }
        int choix = Interaction.lireUnEntier(1, listeIndices.size());

        return indicePourVol.get(choix);
    }

    /**
     * Renvoie les indices de tous les personnages de rang < 1 du jeu sauf le voleur
     */
    private ArrayList<Integer> indicesPersonnages() {

        ArrayList<Integer> listeIndice = new ArrayList<Integer>();

        for (int i = 0; i < this.plateauDeJeu.getNombrePersonnages(); i++) {
            // S'il s'agit du voleur ou si le personage a un rang >= 1
            if (this.plateauDeJeu.getPersonnage(i).getNom().equals(this.nom)
                    || this.plateauDeJeu.getPersonnage(i).getRang() >= 1)
                continue;
            else { // sinon on l'ajoute a la liste
                listeIndice.add(i);
            }
        }
        return listeIndice;
    }
}
