package com.citadelles.models.touslespersonnages;

import com.citadelles.models.Personnage;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;

public class Marchande extends Personnage {
    public Marchande() {
        super();
    }

    public Marchande(String nom, int rang, String caracteristiques) {
        super(nom, rang, caracteristiques);
    }

    /**
     * Son pouvoir est de recevoir une pièce d’or supplémentaire
     */
    @Override
    public void utiliserPouvoir() {
        this.joueur.setTresor(1);
    }

    @Override
    public void utiliserPouvoirAvatar() {

    }

    @Override
    public void percevoirRessourcesSpecifiques() {
        int nbrQuartiersCommercant = 0;
        for (int i = 0; i < this.joueur.nbrQuartiersDansCite(); i++) {
            if (this.joueur.getCite()[i].getType_quartiers() == TYPE_QUARTIERS.COMMERCANT)
                nbrQuartiersCommercant++;
        }
        this.joueur.setTresor(nbrQuartiersCommercant);
        System.out.println("Ressources percues : " + nbrQuartiersCommercant);
    }
}
