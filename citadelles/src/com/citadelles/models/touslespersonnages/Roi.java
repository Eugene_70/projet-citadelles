package com.citadelles.models.touslespersonnages;

import com.citadelles.models.Personnage;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;
import lombok.Data;

@Data
public class Roi extends Personnage {

    public Roi() {
        super();
    }

    public Roi(String nom, int rang, String caracteristiques) {
        super(nom, rang, caracteristiques);
    }

    @Override
    public void utiliserPouvoir() {
        this.joueur.setPossedeCouronne(true);
        System.out.println("Je prends la couronne");
    }

    @Override
    public void utiliserPouvoirAvatar() {

    }

    @Override
    public void percevoirRessourcesSpecifiques() {
        int nbrQuartiersNobles = 0;
        for (int i = 0; i < this.joueur.nbrQuartiersDansCite(); i++) {
            if (this.joueur.getCite()[i].getType_quartiers() == TYPE_QUARTIERS.NOBLE)
                nbrQuartiersNobles++;
        }
        this.joueur.setTresor(nbrQuartiersNobles);
        System.out.println("Ressources percues : " + nbrQuartiersNobles);
    }
}
