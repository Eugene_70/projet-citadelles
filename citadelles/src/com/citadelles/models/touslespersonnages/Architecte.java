package com.citadelles.models.touslespersonnages;

import com.citadelles.models.Personnage;

public class Architecte extends Personnage {
    public Architecte() {
        super();
    }

    public Architecte(String nom, int rang, String caracteristiques) {
        super(nom, rang, caracteristiques);
    }

    @Override
    public void utiliserPouvoir() {
        System.out.println("Perception de deux cartes supplementaires...");
        this.joueur.ajouterQuartierDansMain(this.plateauDeJeu.getPioche().piocher());
        this.joueur.ajouterQuartierDansMain(this.plateauDeJeu.getPioche().piocher());
    }

    @Override
    public void utiliserPouvoirAvatar() {

    }

}
