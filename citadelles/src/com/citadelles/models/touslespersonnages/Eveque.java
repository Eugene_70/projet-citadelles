package com.citadelles.models.touslespersonnages;

import com.citadelles.models.Personnage;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;

public class Eveque extends Personnage {

    public Eveque() {
        super();
    }

    public Eveque(String nom, int rang, String caracteristiques) {
        super(nom, rang, caracteristiques);
    }

    @Override
    public void utiliserPouvoir() {
        System.out.println("Aucun pouvoir");
    }

    @Override
    public void utiliserPouvoirAvatar() {

    }

    @Override
    public void percevoirRessourcesSpecifiques() {
        int nbrQuartiersRelgieux = 0;
        for (int i = 0; i < this.joueur.nbrQuartiersDansCite(); i++) {
            if (this.joueur.getCite()[i].getType_quartiers() == TYPE_QUARTIERS.RELIGIEUX)
                nbrQuartiersRelgieux++;
        }
        this.joueur.setTresor(nbrQuartiersRelgieux);
        System.out.println("Ressources percues : " + nbrQuartiersRelgieux);
    }
}
