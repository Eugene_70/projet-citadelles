package com.citadelles.models.touslespersonnages;

import com.citadelles.controllers.Interaction;
import com.citadelles.models.Personnage;
import com.citadelles.models.Quartier;
import com.citadelles.models.enumerations.TYPE_QUARTIERS;

import java.util.ArrayList;

public class Condottiere extends Personnage {
    public Condottiere() {
        super();
    }

    public Condottiere(String nom, int rang, String caracteristiques) {
        super(nom, rang, caracteristiques);
    }

    /**
     * Le condottiere reçoit une pièce d’or par quartier militaire dans sa cité.
     */
    @Override
    public void percevoirRessourcesSpecifiques() {
        int nbrQuartiersMilitaire = 0;
        for (int i = 0; i < this.joueur.nbrQuartiersDansCite(); i++) {
            if (this.joueur.getCite()[i].getType_quartiers() == TYPE_QUARTIERS.MILITAIRE)
                nbrQuartiersMilitaire++;
        }
        this.joueur.setTresor(nbrQuartiersMilitaire);
        System.out.println("Ressources percues : " + nbrQuartiersMilitaire);
    }

    @Override
    public void utiliserPouvoir() {

        System.out.println("Voulez-vous utiliser votre pouvoir de destruction ? (Oui/Non)");
        if (Interaction.lireOuiOuNon()) {

            int[] indicePersonnage = choisirPersonnageADestruction();
            Quartier quartier = this.plateauDeJeu.getPersonnage(indicePersonnage[0]).getJoueur().getCite()[indicePersonnage[1]];
            System.out.println("Voulez-vous vraiment le detruire ? (Oui/Non)");
            if (Interaction.lireOuiOuNon())
                if (quartier.getCoutConstruction() - 1 >= this.joueur.getTresor()) {
                    this.plateauDeJeu.getPersonnage(indicePersonnage[0]).getJoueur().retirerQuartierDansCite(quartier.getNom());
                    System.out.println("Quartier détruit.");
                    this.joueur.setTresor(quartier.getCoutConstruction() - 1);
                    System.out.println("Vous avez maintenant : " + this.joueur.getTresor());
                } else {
                    System.out.println("Votre trésor n’est pas suffisant");
                    utiliserPouvoir();
                }
            else
                utiliserPouvoir();
        } else
            return; // fin d'utiliserPouvoir
    }

    @Override
    public void utiliserPouvoirAvatar() {

    }

    private int[] choisirPersonnageADestruction() {

        ArrayList<Integer> listeIndices = indicesPersonnages();
        // permet de stocker les correspondances entre le numero du personnage et son indice dans la liste des personnages du plateau de jeu
        ArrayList<Integer> indicePourDestructionPersonnage = new ArrayList<Integer>();
        // permet de stocker les correspondances entre les quartiers a choisir pour detruire
        ArrayList<Integer> indicePourDestructionQuartier = new ArrayList<Integer>();

        System.out.println("Voici la liste des joueurs et le contenu de leur cité : ");
        for (int i = 0; i < listeIndices.size(); i++) {
            Personnage perso = this.plateauDeJeu.getPersonnage(listeIndices.get(i));
            indicePourDestructionPersonnage.set(i + 1, listeIndices.get(i));
            System.out.println(i + 1 + "- " + perso.getNom() + " et sa cité : ");
            Quartier quartier = perso.getJoueur().getCite()[i];
            for (int j = 0; j < perso.getJoueur().nbrQuartiersDansCite(); j++)
                System.out.println("\t - " + this.joueur.nbrFoisQuartiersDansCite(quartier.getNom()) +
                        " " + quartier.getNom() + "(cout : " + quartier.getCoutConstruction() + ") ");
        }
        // Choix du personnage
        System.out.println("Pour information, vous avez 2 pièces d’or dans votre trésor");
        System.out.println("Quel est le personnage dont vou voulez detruire le quartier ? ");
        int choixPerso = Interaction.lireUnEntier(1, listeIndices.size());

        // choix du quartier a detruire
        System.out.println("Parmi ses quartier : ");
        Personnage perso = this.plateauDeJeu.getPersonnage(listeIndices.get(choixPerso));
        for (int i = 0; i < perso.getJoueur().nbrQuartiersDansCite(); i++) {
            indicePourDestructionQuartier.set(i + 1, listeIndices.get(i));
            Quartier quartier = perso.getJoueur().getCite()[i];
            System.out.println(i + 1 + "- " + this.joueur.nbrFoisQuartiersDansCite(quartier.getNom()) +
                    " " + quartier.getNom() + "(cout : " + quartier.getCoutConstruction() + ") ");
        }
        System.out.println("Quel quartier voulez vous detruire ? ");
        int choixQuartier = Interaction.lireUnEntier(1, this.plateauDeJeu.getPersonnage(listeIndices.get(choixPerso)).getJoueur().nbrQuartiersDansCite());

        return new int[]{choixPerso, choixQuartier};
    }

    /**
     * Renvoie les indices de tous les personnages
     * Le joueur possédant le personnage
     * de l’évêque ne peut être choisi (sauf s’il est assassiné)
     */
    private ArrayList<Integer> indicesPersonnages() {

        ArrayList<Integer> listeIndice = new ArrayList<Integer>();

        for (int i = 0; i < this.plateauDeJeu.getNombrePersonnages(); i++) {
            // S'il s'agit du condottiere ou de l'eveque non assassine
            if (this.plateauDeJeu.getPersonnage(i).getNom().equals(this.nom)
                    || !this.plateauDeJeu.getPersonnage(i).isAssassine()
                    && this.plateauDeJeu.getPersonnage(i).getNom().equals("Eveque"))
                continue;
            else { // sinon on l'ajoute a la liste
                listeIndice.add(i);
            }
        }
        return listeIndice;
    }
}
