package com.citadelles.models.enumerations;

public enum TYPE_QUARTIERS {
    RELIGIEUX,
    MILITAIRE,
    NOBLE,
    COMMERCANT,
    MERVEILLE
}
