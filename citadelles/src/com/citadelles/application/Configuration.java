package com.citadelles.application;

import com.citadelles.models.Pioche;
import com.citadelles.models.PlateauDeJeu;

public class Configuration {

    // TODO: Implémentez la méthode statique nouvellePioche() qui renvoie une nouvelle pioche
    //  qui comporte les 54 cartes Quartiers des types religieux, militaires, nobles et commençants
    public static Pioche nouvellePioche() {
        // TODO::Initialiser les 54 merveilles
        return null;
    }

    // TODO: Implémentez la méthode statique configurationDeBase(Pioche p) qui renvoie un
    //  nouveau plateau de jeu comportant les 8 personnages et 4 joueurs de la configuration
    //  de base décrite dans la partie 3.1 du document présentation du jeu Citadelles.
    //  Cette méthode ajoutera à la pioche passée en paramètre, les 14 quartiers Merveille
    //  que l’on doit utiliser dans cette configuration de base
    public static PlateauDeJeu configurationDeBase(Pioche pioche) {
        // TODO::Initialiser 8 personnages
        // TODO::Initialiser 4 joueurs
        // TODO::Ajouter a la pioche les 14 quartiers Merveille que l’on doit utiliser dans cette configuration de base
        // TODO::Initialiser le plateau de jeu
        // TODO::Modifier la pioche du plateau de jeu
        return null;
    }
}
